import React, { Component } from 'react';
import './App.css';

import TaskForm from './components/TaskForm';
import Control from './components/Control';
import TaskList from './components/TaskList';
import Demo from './training/demo';
import { connect } from 'react-redux';
import * as actions from './actions/index';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: {
                name: '',
                status: -1
            }
        }
    }
    UNSAFE_componentWillMount() {
        if (localStorage && localStorage.getItem('tasks')) {
            var tasks = JSON.parse(localStorage.getItem('tasks'));
            this.setState({
                tasks: tasks
            })
        }
    }
    onGenerateData = () => {
        var tasks = [
            {
                id: this.generateID(),
                name: 'AAA',
                status: true
            },
            {
                id: this.generateID(),
                name: 'BBB',
                status: false
            },
            {
                id: this.generateID(),
                name: 'CCC',
                status: true
            },
        ]
        this.setState({
            tasks: tasks
        })
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }
    onToggleForm = () => {
        this.props.onToggleForm();
    }
    onShowForm = () => {
        this.props.onShowForm();
    }
    render() {
        var { isDisplayForm } = this.props;
        return (
            <div className="container">
                <div className="row">
                    <div className={isDisplayForm ? 'col-md-4' : ''}>
                        <TaskForm />
                    </div>
                    <div className={isDisplayForm ? 'col-md-8' : 'col-md-12'}>
                        <button onClick={this.onToggleForm} className="btn btn-primary space-b">Thêm công việc</button>
                        <button className="btn btn-primary space-b space-l" onClick={this.onGenerateData}>Generate data</button>
                        <Control />
                        <div className="card">
                            <div className="card-header">
                                Bảng dữ liệu
                            </div>
                            <div className="card-body">
                                <TaskList />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isDisplayForm: state.isDisplayForm
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onToggleForm: () => {
            dispatch(actions.toggleForm());
        },
        onShowForm: () => {
            dispatch(actions.openForm());
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
