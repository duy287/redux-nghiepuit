import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from './../actions/index';

class TaskItem extends Component {
    onUpdateStatus = () => {
        this.props.onUpdateStatus(this.props.task.id);
    }
    onDelete = () => {
        this.props.onDeleteTask(this.props.task.id)
    }
    onUpdate = () => {
        this.props.onUpdate(this.props.task.id);
    }
    onEditTask = () => {
        this.props.onShowForm();
        this.props.onEditTask(this.props.task);
    }
    render() {
        var { task, index } = this.props;
        return (
            <tr>
                <th scope="row">{index + 1}</th>
                <td>{task.name}</td>
                <td>
                    <span
                        className={task.status === true ? 'badge badge-success' : 'badge badge-danger'}
                        onClick={this.onUpdateStatus}
                    >
                        {task.status === true ? 'Kích hoạt' : 'Ẩn'}
                    </span>
                </td>
                <td>
                    <button onClick={this.onDelete} className="btn btn-danger btn-sm" type="button">Delete</button>
                    <button onClick={this.onEditTask} className="btn btn-success btn-sm space-l" type="button">Edit</button>
                </td>
            </tr>
        );
    }
}

const mapStateToProps = (state) => {
    return {}
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onDeleteTask: (id) => {
            dispatch(actions.deleteTask(id));
        },
        onUpdateStatus: (id) => {
            dispatch(actions.updateStatus(id));
        },
        onShowForm: () => {
            dispatch(actions.openForm());
        },
        onEditTask: (task) => {
            dispatch(actions.editTask(task));
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TaskItem);