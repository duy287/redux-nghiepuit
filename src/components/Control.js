import React, { Component } from 'react';
import Search from './Search';
import Sort from './Sort';

class Control extends Component{
    render(){
        return (
            <div className="row space-b">
                <Search/>
                <Sort/>
            </div>
        );
    }
}

export default Control;