import React, { Component } from 'react';
import TaskItem from './TaskItem';
import { connect } from 'react-redux';
import * as action from '../actions/index';

class TaskList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            filterName: '',
            filterStatus: -1 // all: -1, active: 1, deactive: 0
        }
    }

    onChange = (event) => {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        var filter = {
            'name': name === 'filterName' ? value : this.state.filterName,
            'status': name === 'filterStatus' ? value : this.state.filterStatus
        };
        this.props.onFilterTable(filter);
        this.setState({
            [name]: value
        });
    }
    render() {
        var { tasks, filterTable } = this.props;

        //------------ Filter -----------//
        if (filterTable) {
            if (filterTable.name) {
                tasks = tasks.filter((task) => {
                    return (task.name.toLowerCase().indexOf(filterTable.name) !== -1)
                })
            }
            tasks = tasks.filter((task) => {
                if (filterTable.status === -1)
                    return task;
                else
                    return task.status === (filterTable.status === 1 ? true : false);
            });
        }

        var elmTasks = tasks.map((task, index) => {
            return <TaskItem
                key={task.id}
                index={index}
                task={task}
            />
        })
        return (
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tên</th>
                        <th scope="col">Trạng thái</th>
                        <th scope="col">Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th></th>
                        <td><input name="filterName" value={this.state.filterName} onChange={this.onChange} type="text" className="form-control" /></td>
                        <td>
                            <select name="filterStatus" value={this.state.filterStatus} onChange={this.onChange} className="form-control">
                                <option value={-1}>All</option>
                                <option value={1}>Active</option>
                                <option value={0}>Deactive</option>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                    {elmTasks}
                </tbody>
            </table>
        );
    }
}

// Lên Store lấy state và đổ dữ liệu ra
// chuyển các state của store (trong reducer) thành các props của component để sử dụng
const mapStateToProps = (state) => {
    return {
        tasks: state.tasks,
        filterTable: state.filterTable
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onFilterTable: (filter) => {
            dispatch(action.filterTask(filter))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TaskList);