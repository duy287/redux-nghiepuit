import { createStore } from 'redux';
import { status, sort } from "./actions/index";
import myReducer from './reducers/index';

const store = createStore(myReducer);

//-------------Action 1 ----------//
store.dispatch(status());

//--------------Action 2----------//
store.dispatch(sort({
    by: 'name',
    value: -1
}));
