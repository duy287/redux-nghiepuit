import { combineReducers } from 'redux';
import * as types from './../constants/ActionTypes';

var data = JSON.parse(localStorage.getItem('tasks'));
var initalState = data ? data : [];

var s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}
var generateID = () => {
    return s4() + s4() + s4() + s4();
}

var findIndex = (tasks, id) => {
    var result = -1;
    tasks.forEach((task, index) => {
        if (task.id === id) {
            result = index;
        }
    });
    return result;
}

var myReducer = (state = initalState, action) => {
    switch (action.type) {
        case types.LIST_ALL:
            return state;

        case types.ADD_TASK:
            console.log(action);
            var newTask = {
                id: generateID(),
                name: action.task.name,
                status: action.task.status == 'true' ? true : false
            }
            state.push(newTask);
            localStorage.setItem('tasks', JSON.stringify(state));
            return [...state];

        case types.DELETE_TASK:
            state = state.filter((task) => {
                return task.id !== action.id;
            });
            localStorage.setItem('tasks', JSON.stringify(state));
            return [...state];

        case types.UPDATE_STATUS_TASK:
            console.log(action);
            var id = action.id;
            var index = findIndex(state, id);

            state[index] = {
                ...state[index], //copy
                status: !state[index].status //và thay đổi status
            }
            localStorage.setItem('tasks', JSON.stringify(state));
            return [...state];

        case types.UPDATE_TASK:
            var id = action.task.id;
            var index = findIndex(state, id);
            state[index] = {
                ...state[index], //copy
                name: action.task.name,
                status: action.task.status //và thay đổi status
            }
            return [...state];

        default:
            return state;
    }
}
export default myReducer;